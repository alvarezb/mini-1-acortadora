import webapp


form = """
    <form action="" method="POST">
            <p>Introduce la url: <input type="text" name="url"/>
                url acortada: <input type="text" name="short"/>
                <input type="submit" value="Enviar url"/>
    </form>
"""
# Nombre url acortada: <
class shortenerApp (webapp.webApp):

    content = {}

    def short_urls(self):
        inf: str = ""
        for url in self.content:
            inf = inf + '<p>' + self.content[url] + "=" + url[1:] + '</p>'
        return inf


    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo,recurso,cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

               Finds the HTML text corresponding to the resource name,
               ignoring requests for resources not in the dictionary.
               """
        metodo, ResourceName, cuerpo = parsedRequest

        if metodo == "POST":

            contenido = cuerpo.split('&') #Me guardo los datos

            urlaux = contenido[0].split('=')[1]
            shortaux = contenido[1].split('=')[1]
            urlverdadera = urlaux.replace('%3A', ':').replace('%2F', '/') #Reemplazar los campos

            # Si los campos estan vacios
            if urlverdadera == "" and shortaux == "":
                httpCode = "404 not Found"
                htmlBody = "<html><body><p>Error,debe rellenar los dos campos</p></body></html>"

                return(httpCode, htmlBody)

            # Si la url no viene con https:// o http://
            if urlverdadera[:7] != "http://" and urlverdadera[:8] != "https://":
                urlverdadera = "https://" + urlverdadera
                short = '/' + shortaux #url acortada,le tengo que añadir la barra
                self.content[short] = urlverdadera #añado al diccionario

                httpCode = "200 OK"
                htmlBody = "<html><body>url: " + "<a href='" + urlverdadera + \
                     "'>" + urlverdadera + "</a><br>" + "Short: " + \
                     "<a href='" + self.content[short] + "'>" + \
                     short + "</a></body></html>"
                return (httpCode, htmlBody)
            else:
                short = '/' + shortaux
                self.content[short] = urlverdadera #Añado al diccionario
                httpCode = "200 OK"
                htmlBody = "<html><body>url: " + "<a href='" + urlverdadera + \
                           "'>" + urlverdadera + "</a><br>" + "Short: " + \
                           "<a href='" + self.content[short] + "'>" + \
                           short + "</a></body></html>"
                return (httpCode, htmlBody)

        # Esto es el GET
        if ResourceName == "/":
            httpCode = "200 OK"
            htmlBody = "<html><body><p>" + form + "<p> URLS ya acortadas:</p>" + \
                        "<p>" + self.short_urls() + "</p>" + \
                        "</body></html>"
        elif ResourceName in self.content: #Si el recurso se encuentra en nuestro diccionario,que nos redirija
            httpCode = "302 Found"
            htmlBody = "<html><body><meta http-equiv='refresh' content='2;" + \
                       "url=" + self.content[ResourceName] + "'></body></html>"
        else:
            httpCode = "404 not Found"
            htmlBody = "<html><body><p>Recurso: " + ResourceName + \
                       " no disponible,</body></html>"

        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = shortenerApp("localhost", 1234)